package org.quadcore.bookie;


import java.util.ArrayList;

public class Model_Category
{

    private String categoryName;
    private ArrayList < Model_Book > books;


    /**
     * Constructor
     *
     * @param categoryName  Category name
     * @param books         List of the books
     */
    public Model_Category(String categoryName, ArrayList < Model_Book > books) {
        this.categoryName = categoryName;
        this.books = books;
    }


    // Getters

    public String getCategoryName() {
        return categoryName;
    }

    public ArrayList<Model_Book> getBooks() {
        return books;
    }


    // Setters


    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public void setBooks(ArrayList<Model_Book> books) {
        this.books = books;
    }

}
