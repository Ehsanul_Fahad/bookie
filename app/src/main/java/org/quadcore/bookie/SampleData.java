package org.quadcore.bookie;

import java.util.ArrayList;
import java.util.Arrays;

public class SampleData
{

    private static final SampleData ourInstance = new SampleData();
    private  ArrayList < Model_Category > categories = new ArrayList<>();

    public static SampleData getInstance()
    {
        return ourInstance;
    }

    private SampleData()
    {
        categories.add(new Model_Category("History", new ArrayList<>( Arrays.asList(
                new Model_Book("The English and Their History", "History", "Robert Tombs", null, null, 914, 1828, 5),
                new Model_Book("SPQR: A History of Ancient Rome", "History", "Mary Beard", null, null, 609, 1218, 11),
                new Model_Book("Headstrong - 52 Women Who Changed Science and the World", "History", "Rachel Swaby", null, null, 792, 1584, 2),
                new Model_Book("Beautiful Idiots and Brilliant Lunatics", "History", "Rob Baker", null, null, 914, 1828, 9),
                new Model_Book("The Sea and Civilisation: A Maritime History of the World", "History", "Lincoln Paine", null, null, 1219, 2438, 3),
                new Model_Book("Stalin's Englishman", "History", "Andrew Lownie", null, null, 609, 1218, 20)
        ) )));

        categories.add(new Model_Category("Science Fiction", new ArrayList<>( Arrays.asList(
                new Model_Book("Before Mars", "Science Fiction", "Emma Newman", null, null, 1000, 2000, 3),
                new Model_Book("Artemis", "Science Fiction", "Andy Weir", null, null, 2000, 4000, 5),
                new Model_Book("Provenance", "Science Fiction", "Ann Leckie", null, null, 3000, 6000, 7),
                new Model_Book("Leviathan Wakes", "Science Fiction", "James S.A. Corey", null, null, 4000, 8000, 9),
                new Model_Book("Aurora", "Science Fiction", "Kim Stanley Robinson", null, null, 5000, 10000, 11),
                new Model_Book("The Martian Chronicles", "Science Fiction", "Ray Bradbury", null, null, 6000, 12000, 20)
        ) )));

        categories.add(new Model_Category("History", new ArrayList<>( Arrays.asList(
                new Model_Book("The English and Their History", "History", "Robert Tombs", null, null, 914, 1828, 5),
                new Model_Book("SPQR: A History of Ancient Rome", "History", "Mary Beard", null, null, 609, 1218, 11),
                new Model_Book("Headstrong - 52 Women Who Changed Science and the World", "History", "Rachel Swaby", null, null, 792, 1584, 2),
                new Model_Book("Beautiful Idiots and Brilliant Lunatics", "History", "Rob Baker", null, null, 914, 1828, 9),
                new Model_Book("The Sea and Civilisation: A Maritime History of the World", "History", "Lincoln Paine", null, null, 1219, 2438, 3),
                new Model_Book("Stalin's Englishman", "History", "Andrew Lownie", null, null, 609, 1218, 20)
        ) )));

        categories.add(new Model_Category("Science Fiction", new ArrayList<>( Arrays.asList(
                new Model_Book("Before Mars", "Science Fiction", "Emma Newman", null, null, 1000, 2000, 3),
                new Model_Book("Artemis", "Science Fiction", "Andy Weir", null, null, 2000, 4000, 5),
                new Model_Book("Provenance", "Science Fiction", "Ann Leckie", null, null, 3000, 6000, 7),
                new Model_Book("Leviathan Wakes", "Science Fiction", "James S.A. Corey", null, null, 4000, 8000, 9),
                new Model_Book("Aurora", "Science Fiction", "Kim Stanley Robinson", null, null, 5000, 10000, 11),
                new Model_Book("The Martian Chronicles", "Science Fiction", "Ray Bradbury", null, null, 6000, 12000, 20)
        ) )));

        categories.add(new Model_Category("History", new ArrayList<>( Arrays.asList(
                new Model_Book("The English and Their History", "History", "Robert Tombs", null, null, 914, 1828, 5),
                new Model_Book("SPQR: A History of Ancient Rome", "History", "Mary Beard", null, null, 609, 1218, 11),
                new Model_Book("Headstrong - 52 Women Who Changed Science and the World", "History", "Rachel Swaby", null, null, 792, 1584, 2),
                new Model_Book("Beautiful Idiots and Brilliant Lunatics", "History", "Rob Baker", null, null, 914, 1828, 9),
                new Model_Book("The Sea and Civilisation: A Maritime History of the World", "History", "Lincoln Paine", null, null, 1219, 2438, 3),
                new Model_Book("Stalin's Englishman", "History", "Andrew Lownie", null, null, 609, 1218, 20)
        ) )));

        categories.add(new Model_Category("Science Fiction", new ArrayList<>( Arrays.asList(
                new Model_Book("Before Mars", "Science Fiction", "Emma Newman", null, null, 1000, 2000, 3),
                new Model_Book("Artemis", "Science Fiction", "Andy Weir", null, null, 2000, 4000, 5),
                new Model_Book("Provenance", "Science Fiction", "Ann Leckie", null, null, 3000, 6000, 7),
                new Model_Book("Leviathan Wakes", "Science Fiction", "James S.A. Corey", null, null, 4000, 8000, 9),
                new Model_Book("Aurora", "Science Fiction", "Kim Stanley Robinson", null, null, 5000, 10000, 11),
                new Model_Book("The Martian Chronicles", "Science Fiction", "Ray Bradbury", null, null, 6000, 12000, 20)
        ) )));

    }

    public ArrayList<Model_Category> getCategories()
    {
        return categories;
    }

}
