package org.quadcore.bookie.adapters;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.quadcore.bookie.Model_Category;
import org.quadcore.bookie.R;

import java.util.List;


public class Adapter_RV_Home extends Adapter
{

    private Context context;
    private List < Model_Category > categories;


    public Adapter_RV_Home(List < Model_Category > categories, Context context) {
        this.categories = categories;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CategoryViewHolder(
                LayoutInflater.from( parent.getContext() )
                        .inflate(R.layout.recycler_view_item_category, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final Model_Category currentCategory = categories.get(position);
        final CategoryViewHolder itemHolder = (CategoryViewHolder) holder;

        itemHolder.textView_categoryTitle.setText(currentCategory.getCategoryName());
        itemHolder.recyclerView_books.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        itemHolder.recyclerView_books.setAdapter(new Adapter_RV_Books(currentCategory.getBooks(), context));
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }



    class CategoryViewHolder extends RecyclerView.ViewHolder
    {

        TextView textView_categoryTitle;
        RecyclerView recyclerView_books;

        CategoryViewHolder(View itemView) {
            super(itemView);

            textView_categoryTitle = itemView.findViewById(R.id.textView_categoryTitle);
            recyclerView_books = itemView.findViewById(R.id.recyclerView_books);
        }

    }

}

