package org.quadcore.bookie.adapters;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.quadcore.bookie.Model_Book;
import org.quadcore.bookie.R;

import java.util.List;
import java.util.Locale;


public class Adapter_RV_Books extends Adapter
{

    private List < Model_Book > books;
    private Context context;


    public Adapter_RV_Books(List < Model_Book > books, Context context) {
        this.books = books;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new BookViewHolder(
                LayoutInflater.from( parent.getContext() )
                        .inflate(R.layout.recycler_view_item_book, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final Model_Book currentBook = books.get(position);
        final BookViewHolder itemHolder = (BookViewHolder) holder;

        itemHolder.textView_bookName.setText( currentBook.getBookName() );
        itemHolder.textView_authorName.setText( currentBook.getAuthorName_1() );
        itemHolder.textView_coins.setText( String.format(Locale.getDefault(), "%d\ncoins", currentBook.getPriceInCoins()) );
        itemHolder.textView_days.setText( String.format(Locale.getDefault(), "%d\ndays", currentBook.getLendingDays()) );

        switch (position % 3) {
            case 0:
                itemHolder.linearLayout_bookItem.setBackgroundColor(ContextCompat.getColor(context, R.color.colorBookItemBG_1));
                break;

            case 1:
                itemHolder.linearLayout_bookItem.setBackgroundColor(ContextCompat.getColor(context, R.color.colorBookItemBG_2));
                break;

            case 2:
                itemHolder.linearLayout_bookItem.setBackgroundColor(ContextCompat.getColor(context, R.color.colorBookItemBG_3));
                break;
        }
    }

    @Override
    public int getItemCount() {
        return books.size();
    }



    class BookViewHolder extends RecyclerView.ViewHolder
    {

        TextView textView_bookName, textView_authorName, textView_coins, textView_days;
        LinearLayout linearLayout_bookItem;

        BookViewHolder(View itemView) {
            super(itemView);

            textView_bookName = itemView.findViewById(R.id.textView_bookName);
            textView_authorName = itemView.findViewById(R.id.textView_authorName);
            textView_coins = itemView.findViewById(R.id.textView_coins);
            textView_days = itemView.findViewById(R.id.textView_days);

            linearLayout_bookItem = itemView.findViewById(R.id.linearLayout_bookItem);
        }

    }

}

