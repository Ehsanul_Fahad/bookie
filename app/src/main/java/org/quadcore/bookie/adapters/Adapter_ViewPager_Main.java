package org.quadcore.bookie.adapters;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;


public class Adapter_ViewPager_Main extends FragmentPagerAdapter {

    private ArrayList < String > titles;
    private ArrayList < Fragment > fragments;

    public Adapter_ViewPager_Main(FragmentManager fm, ArrayList < String > titles, ArrayList <Fragment> fragments) {
        super(fm);
        this.titles = titles;
        this.fragments = fragments;
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        if (position < titles.size()) return titles.get(position);
        else return " ";
    }
}
