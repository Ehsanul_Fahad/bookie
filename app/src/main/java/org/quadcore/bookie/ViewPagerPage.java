package org.quadcore.bookie;


import android.support.annotation.IdRes;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;

public class ViewPagerPage {

    private String title = null;
    private Fragment fragment = null;
    private int bottomNavigationItemId = 0;
    private int navigationDrawerItemId = 0;

    // Constructor
    public ViewPagerPage(Fragment fragment)
    {
        this.fragment = fragment;
    }


    // Getters

    public String getTitle()
    {
        return title;
    }

    public Fragment getFragment()
    {
        return fragment;
    }

    public int getBottomNavigationItemId()
    {
        return bottomNavigationItemId;
    }

    public int getNavigationDrawerItemId()
    {
        return navigationDrawerItemId;
    }


    // Setters

    public ViewPagerPage setTitle(String title)
    {
        this.title = title;
        return this;
    }

    public ViewPagerPage setFragment(Fragment fragment)
    {
        this.fragment = fragment;
        return this;
    }

    public ViewPagerPage setBottomNavigationItemId(@IdRes int bottomNavigationItemId)
    {
        this.bottomNavigationItemId = bottomNavigationItemId;
        return this;
    }

    public ViewPagerPage setNavigationDrawerItemId(@IdRes int navigationDrawerItemId)
    {
        this.navigationDrawerItemId = navigationDrawerItemId;
        return this;
    }
}
