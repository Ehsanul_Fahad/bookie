package org.quadcore.bookie.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ActionMenuView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import org.quadcore.bookie.fragments.HomeFragment;
import org.quadcore.bookie.R;
import org.quadcore.bookie.ViewPagerInflater;
import org.quadcore.bookie.ViewPagerPage;

import java.lang.reflect.Field;

public class MainActivity extends AppCompatActivity
{

    private Toolbar toolbar_main;
    private ImageButton imageButton_search;
    private ActionMenuView actionMenuView_main;
    private SearchView searchView_main;
    private boolean searchViewShown = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar_main = findViewById(R.id.toolbar_main);
        setSupportActionBar(toolbar_main);

        /*actionMenuView_main = findViewById(R.id.actionMenuView_main);
        actionMenuView_main.setOnMenuItemClickListener(new ActionMenuView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                return onOptionsItemSelected(menuItem);
            }
        });*/

        BottomNavigationViewEx navigation = findViewById(R.id.navigation);
        navigation.setItemIconTintList(null);
        navigation.setIconSize(44, 44);

        ViewPager viewPager_main = findViewById(R.id.viewPager_main);

        new ViewPagerInflater(this, viewPager_main)
                .addPage(
                        new ViewPagerPage(new HomeFragment())
                                .setBottomNavigationItemId(R.id.navigation_home)
                                .setTitle(null)
                )
                .setBottomNavigationBar(navigation)
                .inflateAll();

        initSearchView();
    }


    public void initSearchView()
    {
        searchView_main = findViewById(R.id.searchView_main);

        /*
        // Enable Submit button
        searchView_main.setSubmitButtonEnabled(true);
        */

        // Change search close button image
        ImageView closeButton = searchView_main.findViewById(R.id.search_close_btn);
        closeButton.setImageResource(R.drawable.ic_close_24dp);

        // Set hint and the text colors
        final EditText txtSearch = searchView_main.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        txtSearch.setHint("Search by book name or author name");
        txtSearch.setHintTextColor(ContextCompat.getColor(getApplicationContext(), android.R.color.darker_gray));
        txtSearch.setTextColor(getResources().getColor(android.R.color.white));

        // Set the cursor
        AutoCompleteTextView searchTextView = (AutoCompleteTextView) txtSearch;

        try {
            Field mCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
            mCursorDrawableRes.setAccessible(true);
            mCursorDrawableRes.set(searchTextView, R.drawable.white_cursor);
        } catch (Exception e) {
            e.printStackTrace();
        }

        searchView_main.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                callSearch(query);
                searchView_main.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                callSearch(newText);
                return true;
            }

            private void callSearch(String query) {
                //Do searching
                Log.i("query", "" + query);

            }

        });

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Clear the text from EditText view
                txtSearch.setText("");

                //Clear query
                searchView_main.setQuery("", false);
                //Collapse the action view
                //searchView_main.onActionViewCollapsed();
                //Collapse the search widget
                //searchView_main.collapseActionView();

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                    circleReveal(searchView_main, !searchViewShown);
                else
                    searchView_main.setVisibility(searchViewShown ? View.INVISIBLE : View.VISIBLE);

                searchView_main.setIconified(!searchView_main.isIconified());
                searchViewShown = !searchViewShown;
            }
        });

        searchView_main.setVisibility(View.INVISIBLE);

        imageButton_search = findViewById(R.id.imageButton_search);
        imageButton_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                    circleReveal(searchView_main, !searchViewShown);
                else
                    searchView_main.setVisibility(searchViewShown ? View.INVISIBLE : View.VISIBLE);

                searchView_main.setIconified(!searchView_main.isIconified());
                searchViewShown = !searchViewShown;
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void circleReveal(final View view, final boolean isShow)
    {
        int width = view.getWidth();

        int cx = (int)imageButton_search.getX() + (imageButton_search.getWidth() / 2);
        int cy = (int)imageButton_search.getY() + (imageButton_search.getHeight() / 2);

        Animator anim;
        if (isShow)
            anim = ViewAnimationUtils.createCircularReveal(view, cx, cy, 0,(float)width);
        else
            anim = ViewAnimationUtils.createCircularReveal(view, cx, cy, (float)width, 0);

        anim.setDuration((long) 220);

        // make the view invisible when the animation is done
        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                if(!isShow)
                {
                    super.onAnimationEnd(animation);
                    view.setVisibility(View.INVISIBLE);
                }
            }
        });

        // make the view visible and start the animation
        if(isShow)
            view.setVisibility(View.VISIBLE);

        // start the animation
        anim.start();
    }

    @Override
    public void onBackPressed() {
        if (!searchView_main.isIconified()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                circleReveal(searchView_main, !searchViewShown);
            else
                searchView_main.setVisibility(searchViewShown ? View.INVISIBLE : View.VISIBLE);

            searchView_main.setIconified(true);
            searchViewShown = !searchViewShown;

        } else {
            super.onBackPressed();
        }
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_toolbar, menu);
        // Retrieve the SearchView and plug it into SearchManager
        final SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();

        /*searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                //some operation
            }
        });*--------------------------------------------/
        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //some operation
            }
        });
        EditText searchPlate = (EditText) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchPlate.setHint("Search by book name or author name");
        View searchPlateView = searchView.findViewById(android.support.v7.appcompat.R.id.search_plate);
        //searchPlateView.setBackgroundColor(ContextCompat.getColor(this, android.R.color.transparent));
        // use this method for search process
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // use this method when query submitted
                Toast.makeText(getApplicationContext(), query, Toast.LENGTH_SHORT).show();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                // use this method for auto complete search process
                return false;
            }
        });

        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        if (searchManager != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        }

        return true;
    }*/

}
