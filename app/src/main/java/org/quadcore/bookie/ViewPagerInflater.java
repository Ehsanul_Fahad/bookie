package org.quadcore.bookie;


import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import org.quadcore.bookie.adapters.Adapter_ViewPager_Main;

import java.util.ArrayList;


public class ViewPagerInflater {

    private int currentItem = -1;
    private boolean titleTextAllCaps = true;
    private ArrayList < Fragment > fragments;
    private ArrayList < String > titles;
    private ArrayList < Integer > bottomNavigationItemIds;

    private AppCompatActivity activity;
    private ViewPager viewPager;
    private Adapter_ViewPager_Main adapterViewPagerMain;
    private BottomNavigationViewEx bottomNavigationViewEx = null;
    private Toolbar toolbar;


    /**
     * Constructor
     *
     * @param activity  the activity that holds the view pager
     * @param viewPager the view pager object we are trying to inflate
     */
    public ViewPagerInflater(AppCompatActivity activity, ViewPager viewPager)
    {
        this.activity = activity;
        this.viewPager = viewPager;

        fragments = new ArrayList<>();
        titles = new ArrayList<>();
        bottomNavigationItemIds = new ArrayList<>();
    }


    /**
     * @param viewPagerPage the page we want to add to the view pager
     * @return              current ViewPagerInflater object
     */
    public ViewPagerInflater addPage(ViewPagerPage viewPagerPage)
    {
        titles.add( viewPagerPage.getTitle() );
        fragments.add( viewPagerPage.getFragment() );
        bottomNavigationItemIds.add( viewPagerPage.getBottomNavigationItemId() );
        return this;
    }

    /**
     * @param bottomNavigationViewEx    the bottom navigation bar we want to link to the view pager
     * @return                          current ViewPagerInflater object
     */
    public ViewPagerInflater setBottomNavigationBar(BottomNavigationViewEx bottomNavigationViewEx)
    {
        this.bottomNavigationViewEx = bottomNavigationViewEx;
        return this;
    }

    /**
     * @param toolbar   the toolbar we want to link to the view pager
     * @return          current ViewPagerInflater object
     */
    public ViewPagerInflater setToolbar(Toolbar toolbar)
    {
        this.toolbar = toolbar;
        return this;
    }

    /**
     * @param currentItem   preselected item position
     * @return              current ViewPagerInflater object
     */
    public ViewPagerInflater setCurrentItem(int currentItem)
    {
        this.currentItem = currentItem;
        return this;
    }

    /**
     * @param titleTextAllCaps  if the titles in the toolbar should be shown in all capital letter or not
     * @return                  current ViewPagerInflater object
     */
    public ViewPagerInflater setTitleTextAllCaps(boolean titleTextAllCaps) {
        this.titleTextAllCaps = titleTextAllCaps;
        return this;
    }

    /**
     * Method to inflate the view pager
     */
    public void inflateAll()
    {
        adapterViewPagerMain = new Adapter_ViewPager_Main(activity.getSupportFragmentManager(), titles, fragments);
        viewPager.setAdapter(adapterViewPagerMain);
        if (currentItem != -1) viewPager.setCurrentItem( currentItem );

        if (this.bottomNavigationViewEx != null) {
            if (currentItem != -1) bottomNavigationViewEx.setCurrentItem( currentItem );
            bottomNavigationViewEx.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    int len = bottomNavigationItemIds.size();
                    int i;

                    for (i = 0; i < len; i++) {
                        if (bottomNavigationItemIds.get(i) == item.getItemId()) {
                            if (toolbar != null) {
                                if (titleTextAllCaps) toolbar.setTitle(titles.get(i).toUpperCase());
                                else toolbar.setTitle(titles.get(i));
                            }

                            if (viewPager != null)
                                viewPager.setCurrentItem(i, true);

                            break;
                        }
                    }

                    return true;
                }
            });

            for (int i = 0; i < bottomNavigationItemIds.size(); i++) {
                if (i >= titles.size()) break;

                MenuItem menuItem = bottomNavigationViewEx.getMenu()
                        .findItem( bottomNavigationItemIds.get(i) );

                if (menuItem != null) menuItem.setTitle( titles.get(i) );
            }
        }
    }

}
