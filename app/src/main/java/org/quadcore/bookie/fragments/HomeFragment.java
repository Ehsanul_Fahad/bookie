package org.quadcore.bookie.fragments;


import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.quadcore.bookie.R;
import org.quadcore.bookie.SampleData;
import org.quadcore.bookie.adapters.Adapter_RV_Home;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment
{

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);

        RecyclerView recyclerView_home = rootView.findViewById(R.id.recyclerView_home);
        recyclerView_home.setNestedScrollingEnabled(true);
        recyclerView_home.setLayoutManager(new LinearLayoutManager(getContext()));

        if (getContext() != null) {
            DividerItemDecoration itemDecorator = new DividerItemDecoration(getContext(), ((LinearLayoutManager) recyclerView_home.getLayoutManager()).getOrientation());
            Drawable recyclerViewDivider = ContextCompat.getDrawable(getContext(), R.drawable.divider_rv_home);

            if (recyclerViewDivider != null) {
                itemDecorator.setDrawable(recyclerViewDivider);
                recyclerView_home.addItemDecoration(itemDecorator);
            }
        }
        recyclerView_home.setAdapter(new Adapter_RV_Home(SampleData.getInstance().getCategories(), getContext()));


        return rootView;
    }

}
