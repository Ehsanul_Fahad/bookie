package org.quadcore.bookie;


public class Model_Book
{

    private String bookName, category;
    private String authorName_1, authorName_2, authorName_3;
    private int priceInTaka, priceInCoins, lendingDays;


    /**
     * Constructor
     *
     * @param bookName      Name of the book
     * @param category      Category of the book
     * @param authorName_1  1st author's name
     * @param authorName_2  2nd author's name, if exists, or null or empty string
     * @param authorName_3  3rd author's name, if exists, or null or empty string
     * @param priceInTaka   Price of the book in Taka
     * @param priceInCoins  Price of the book in virtual coins
     * @param lendingDays   Amount of days the book is lent for
     */
    public Model_Book(String bookName, String category, String authorName_1, String authorName_2, String authorName_3, int priceInTaka, int priceInCoins, int lendingDays) {
        this.bookName = bookName;
        this.category = category;
        this.authorName_1 = authorName_1;
        this.authorName_2 = authorName_2;
        this.authorName_3 = authorName_3;
        this.priceInTaka = priceInTaka;
        this.priceInCoins = priceInCoins;
        this.lendingDays = lendingDays;
    }


    // Getters

    public String getBookName() {
        return bookName;
    }

    public String getCategory() {
        return category;
    }

    public String getAuthorName_1() {
        return authorName_1;
    }

    public String getAuthorName_2() {
        return authorName_2;
    }

    public String getAuthorName_3() {
        return authorName_3;
    }

    public int getPriceInTaka() {
        return priceInTaka;
    }

    public int getPriceInCoins() {
        return priceInCoins;
    }

    public int getLendingDays() {
        return lendingDays;
    }


    // Setters

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setAuthorName_1(String authorName_1) {
        this.authorName_1 = authorName_1;
    }

    public void setAuthorName_2(String authorName_2) {
        this.authorName_2 = authorName_2;
    }

    public void setAuthorName_3(String authorName_3) {
        this.authorName_3 = authorName_3;
    }

    public void setPriceInTaka(int priceInTaka) {
        this.priceInTaka = priceInTaka;
    }

    public void setPriceInCoins(int priceInCoins) {
        this.priceInCoins = priceInCoins;
    }

    public void setLendingDays(int lendingDays) {
        this.lendingDays = lendingDays;
    }

}
